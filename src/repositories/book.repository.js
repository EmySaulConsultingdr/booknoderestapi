let _book = null
let _format = null

const BaseRepository = require('./base.repository')

class BookRepository extends BaseRepository {
    constructor ({ db }) {
        _book = db['Book']
        _format = db['Format']
        super(_book)
    }

    async getAllWithFormats (pageSize = 5, pageNumber = 1) {
       return super.getAll(pageSize, pageNumber,
         [{ 
             model:_format
        }])
    }

}
module.exports = BookRepository