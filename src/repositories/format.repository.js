let _page = null
let _format = null
const { Op } = require("sequelize");

const BaseRepository = require('./base.repository')

class FormatRepository extends BaseRepository {
    constructor ({ db }) {
        _page = db['Page']
        _format = db['Format']
        super(_format)
    }

    async get(bookId, page, format) {
       return await _format.findOne({
            where: {
                    [Op.and]: [
                      {
                        description: {
                          [Op.like]: '%'+format+'%'
                        }
                      },
                      {
                        bookId: {
                          [Op.eq]: bookId
                        }
                      }
                    ]
            },
            include: [{ 
                model:_page,
                where: {
                  number: {
                    [Op.eq]: page
                  }
                }
           }]
        })
    }

}
module.exports = FormatRepository