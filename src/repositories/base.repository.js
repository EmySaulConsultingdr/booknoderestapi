class BaseRepository {
    constructor (model) {
        this.model = model
    }

    async get (id) {
        return await this.model.findByPk(id)
    }

    async getAll (pageSize = 5, pageNumber = 1, include) {
        const skips = pageSize * (pageNumber - 1)
        return await this.model
            .findAll({
                include: include,
                offset: skips, limit: pageSize,
            })
    }
    
    async create (entity) {
        return await this.model.create(entity)
    }
}

module.exports = BaseRepository