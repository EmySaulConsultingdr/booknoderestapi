module.exports = (err, req, res, next) => {
    const httpStatus = err.status || 500
    console.log('httpStatus', httpStatus)
    
    const message = err.message || 'Internal Server Error'
    res.writeHead(404, message, {'content-type' : 'text/plain'});
    res.end(message);
}