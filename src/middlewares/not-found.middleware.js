module.exports = (req, res, next) => 
{
  res.writeHead(404, 'Resource not found', {'content-type' : 'text/plain'});
  res.end('Resource not found');
}