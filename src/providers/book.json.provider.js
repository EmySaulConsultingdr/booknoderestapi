class BookJsonProvider {
    async getBooks() {
        return [
            {
                title: 'Book2',
                release: new Date(),
                author: 'Aqua',
                cover: 'martha',
                genre: 'Comedia',
                formats: [
                    {
                        description: "txt",
                        contentType: 'text/plain',
                        pages: [
                            {
                                number: 1,
                                content: "Hola"
                            },
                            {
                                number: 2,
                                content: "Como estas"
                            },
                            {
                                number: 3,
                                content: "Hasta luego"
                            }
                        ]
                    },
                    {
                        description: "html",
                        contentType: 'text/html',
                        pages: [
                            {
                                number: 1,
                                content: "<h1>Hola</h1>"
                            },
                            {
                                number: 2,
                                content: "<h1>Como estas</h1>"
                            },
                            {
                                number: 3,
                                content: "<h1>Hasta luego</h1>"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
module.exports = new BookJsonProvider()