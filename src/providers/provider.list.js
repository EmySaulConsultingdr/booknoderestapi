const BookJsonProvider = require('./book.json.provider')
const BookWebProvider = require('./book.web.provider')

const providers = [
    BookJsonProvider,
    BookWebProvider
]

module.exports = providers