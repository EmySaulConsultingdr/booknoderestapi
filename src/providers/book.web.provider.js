class BookWebProvider {
    async getBooks() {
        return [
            {
                title: 'Yordy',
                release: new Date(),
                author: 'Pena',
                cover: 'martha',
                genre: 'Comedia',
                formats: [
                    {
                        description: "Html",
                        contentType: 'text/html',
                        pages: [
                            {
                                number: 1,
                                content: "<h1>Hola</h1>"
                            },
                            {
                                number: 2,
                                content: "<h1>Como estas</h1>"
                            },
                            {
                                number: 3,
                                content: "<h1>Hasta luego</h1>"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
module.exports = new BookWebProvider()