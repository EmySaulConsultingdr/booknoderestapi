const providers = require('./provider.list')

class BookProvider {
    constructor () {
    }

    async getCompleteBooks () {
        let completeBooks = []
        for (const provider of providers) {
            let providerBooks = await provider.getBooks()
            completeBooks = completeBooks.concat(providerBooks)
        }
        return completeBooks
    }

    async getBooks () {
        const completeBooks = await this.getCompleteBooks()
        let ids = {
            bookId: 1,
            formatId: 1,
            pageId: 1,
            books: [],
            formats: [],
            pages: []
        }
        completeBooks.forEach((book)=> {
            this.insertBook(book, ids)
            this.insertFormats(book.formats, ids)
            ids.bookId++;
        })
        return ids
    }

    insertBook(book, ids) {
        const newBook = {
            id: ids.bookId,
            ...book, 
            createdAt: new Date(),
            updatedAt: new Date()
        }
        delete newBook.formats
        ids.books.push(newBook)
    }

    insertFormats(currentBookFormats, ids) {
        currentBookFormats.forEach((format) => {
            const newFormat = {
                id: ids.formatId,
                bookId: ids.bookId,
                description: format.description,
                contentType: format.contentType,
                countPage: format.pages.length,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            ids.formats.push(newFormat)
            this.insertPages(format.pages, ids)
            ids.formatId++;
        })
    }

    insertPages(currentFormatPages, ids) {
        currentFormatPages.forEach((page) => {
            const newPage = {
                id: ids.pageId,
                formatId: ids.formatId,
                content: page.content,
                number: page.number,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            ids.pages.push(newPage)
            ids.pageId++;
        })
    }
}
module.exports = new BookProvider()