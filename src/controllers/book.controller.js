const url = require('url');
const BaseController = require('./base.controller')

let _bookService = null
let _config = null
class BookController extends BaseController {
    constructor ({ config, BookService }) {
        super()
        _bookService = BookService
        _config = config
    }

    async getAll (req, res) {
        const uri = `${_config.PROTOCOL}://${_config.HOST}/${req.url}`
        var params = new url.URL(uri).searchParams;
        super.apiResponse(await _bookService.getAll(params.get('pageSize'), params.get('pageNumber')), req, res)
    }

    async get (req, res) {
        const { bookId, page, format } = req.params;
        const { content, contentType } = await _bookService.get(bookId, page, format)
        super.apiResponse(content, req, res, contentType)
    }

    async create (req, res) {
        super.apiBodyResponse(req, res, async (postBody) => {
           return await _bookService.create(postBody)
        })
    }
}

module.exports = BookController