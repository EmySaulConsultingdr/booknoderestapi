class BaseController {

    apiResponse (obj, req, res, contentType = 'application/json') {
        res.setHeader('Content-Type', contentType)
        if (contentType !== 'application/json') {
            return res.end(obj);
        }
        return res.end(JSON.stringify(obj));
    }

    apiBodyResponse(req, res, callback) { 
        let body = '';
        req.on('data', function (chunk) {
            body += chunk;
        });
        req.on('end', async function () {
            const postBody = JSON.parse(body);
            res.setHeader('Content-Type', 'application/json');
            return res.end(JSON.stringify(callback(postBody)));
        });
    }
}

module.exports = BaseController