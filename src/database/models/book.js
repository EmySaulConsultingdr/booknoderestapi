'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    static associate(models) {
      // define association here
      Book.hasMany(models.Format, {
        foreignKey: 'bookId',
      })
    }
  };
  Book.init({
    title: DataTypes.STRING,
    genre: DataTypes.STRING,
    release: DataTypes.DATE,
    author: DataTypes.STRING,
    cover: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Book',
  });
  return Book;
};