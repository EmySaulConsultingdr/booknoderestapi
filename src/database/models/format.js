'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Format extends Model {
    static associate(models) {
      // define association here
      Format.belongsTo(models.Book, {
        foreignKey: 'bookId',
        onDelete: 'CASCADE'
      })

      Format.hasMany(models.Page, {
        foreignKey: 'formatId',
      })
    }
  };
  Format.init({
    description: DataTypes.STRING,
    contentType: DataTypes.STRING,
    bookId: DataTypes.INTEGER,
    countPage: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Format',
  });
  return Format;
};