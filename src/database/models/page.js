'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Page extends Model {
   
    static associate(models) {
      // define association here
      Page.belongsTo(models.Format, {
        foreignKey: 'formatId',
        onDelete: 'CASCADE'
      })
    }
  };
  Page.init({
    formatId: DataTypes.INTEGER,
    content: DataTypes.STRING,
    number: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Page',
  });
  return Page;
};