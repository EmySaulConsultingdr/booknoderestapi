'use strict';
const booksProviders = require('../../providers')
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const { books, formats, pages } = await booksProviders.getBooks()
    await queryInterface.bulkInsert('Books', books, {});
    await queryInterface.bulkInsert('Formats', formats, {});
    return await queryInterface.bulkInsert('Pages', pages, {});

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Books', null, {});
    await queryInterface.bulkDelete('Formats', null, {});
    return await queryInterface.bulkDelete('Pages', null, {});
  }
};
