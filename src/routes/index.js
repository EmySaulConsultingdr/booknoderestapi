const Router = require('router')
const { ErrorMiddleware, NotFoundMiddleware } = require('../middlewares')

module.exports = function ({ BookRoutes }) {
    const router = Router()
    const apiRoutes = Router();

    apiRoutes.use('/book', BookRoutes)

    router.use('/v1/api/', apiRoutes)

    
    router.use(NotFoundMiddleware)
    router.use(ErrorMiddleware)

    return router
}