const Router = require('router')

module.exports = function ({ BookController }) {
    const router = Router()
    router.get('/', BookController.getAll);
    router.get('/:bookId/page/:page/format/:format', BookController.get);
    router.post('/', BookController.create);
    return router
}