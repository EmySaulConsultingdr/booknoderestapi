let _bookRepository = null
let _formatRepository = null
class BookService {
    constructor ({ BookRepository, FormatRepository }) {
        _bookRepository = BookRepository
        _formatRepository = FormatRepository
    }
    async getAll (pageSize, pageNumber) {
        return await _bookRepository.getAllWithFormats(pageSize, pageNumber )
    }
    
    async get (id, page, format) {
        const { Pages, contentType } = (await _formatRepository.get(id, page, format)) ?? {}
        return { content: Pages?.first()?.content, contentType }
    }

    async create (book) {
        return await _bookRepository.create(book)
    }
}

module.exports = BookService