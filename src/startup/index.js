const http = require('http');
const finalHandler = require('finalhandler');

let _router = null
let _config = null

class Server {
    constructor ({ config, router}) {
        _config = config
        _router = router
    }

    start () {
        return new Promise(resolve => {
            const server = http.createServer(function (req, res) {
                _router(req, res, finalHandler(req, res));
            });
            server.listen(_config.PORT, _config.HOST, () => {
                console.log(_config.APPLICATION_NAME + ' API running on port ' + _config.PORT )
                resolve()
            })
        })
    }
}

module.exports = Server