const { createContainer, asClass, asValue,asFunction } = require('awilix')
const config = require('../config')
const Routes = require('../routes')
const db = require('../database/models')

// routes 
const { BookRoutes } = require('../routes/index.routes')

// repositories 
const { BookRepository, FormatRepository  } = require('../repositories')

// services 
const { BookService  } = require('../services')

// controllers 
const { BookController } = require('../controllers')

const container = createContainer()
const app = require('.')

container.register({
    BookRoutes: asFunction(BookRoutes).singleton(),
})
.register({
    router: asFunction(Routes).singleton(),
    config: asValue(config)
})
.register({
    app: asClass(app).singleton(),
})
.register({
    BookRepository: asClass(BookRepository).singleton(),
    FormatRepository: asClass(FormatRepository).singleton()
})
.register({
    BookService: asClass(BookService).singleton()
})
.register({
    BookController: asClass(BookController).singleton()
})
.register({
    db: asValue(db)
})
module.exports = container